# Composer

Installation du package league/csv via composer.

## Exercice 1
Ecrire un script pour lire le fichier *myTodo.txt* et afficher toutes les tâches de la catégorie **Code**.

## Exercice 2
Ecrire un script pour ajouter vos tâches à faire (état **todo**) dans le fichier *myTodo.txt*.

## Exercice 3
Ecrire un script permettant d’afficher les tâches de votre choix.
Par exemple, afficher seulement les tâches qui sont dans un état **todo** puis **done**.

## Exercice 4
Ecrire un script permettant de modifier l’état d’une tâche.
Par exemple, une tâche dans un état **todo** doit pouvoir passer en état **done**.

## Bonus
Pour aller plus loin avec php CLI ; Installation du package league/climate. Regarder dans un 1er temps la partie de la documentation concernant les arguments.

Ecrire un seul script permettant de prendre en charge les 4 fonctionnalités précédentes.

