PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "user" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        firstname TEXT NOT NULL,
        lastname TEXT NOT NULL
);
INSERT INTO user VALUES(1,'George','Orwell');
INSERT INTO user VALUES(2,'Pablo','Servigne');
INSERT INTO user VALUES(3,'Claude','Bourguignon');
CREATE TABLE IF NOT EXISTS "post" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
	user_id INTEGER NOT NULL,
        title TEXT NOT NULL,
	content TEXT NOT NULL,
	FOREIGN KEY(user_id) REFERENCES user(id)
);
INSERT INTO post VALUES(1,3,'Manifeste pour une agriculture durable',replace('La mondialisation et la “révolution verte” n’ont pas tenu leurs promesses car toutes deux sont des idéologies qui ne tiennent pas compte de la réalité. La première a créé une inégalité entre les mégapoles qui s’enrichissent et les campagnes qui se désertifient et s’appauvrissent. Quant à la deuxième, elle est basée sur une approche simpliste du sol, ramené au rôle de support, et sur une conception industrielle du vivant réduit à une masse de biomolécules et de matières premières ; elle a rendu l’agriculture polluante, destructrice de l’environnement, productrice de malbouffe et incapable d’assurer la sécurité alimentaire de la France comme la survie économique de ses agriculteurs. Désormais, les citoyens français protestent contre ce modèle archaïque et demandent à l’agriculture d’évoluer vers plus de durabilité, en produisant des aliments de qualité et en assurant aux paysans une pérennité de leur métier. Les politiques commencent à peine à prendre conscience de ces problèmes car ils ont abandonné depuis longtemps le développement agricole à l’agro-industrie.\nPrésenter l’agriculture durable comme une solution pour cette France qui maltraite à la fois son agriculture et son industrie, en important sa nourriture et ses produits manufacturés de pays à bas salaires, peut paraître utopique. Or, l’agriculture\ndurable présente deux atouts fondamentaux, que l’humanité, hypnotisée par les hautes technologies, a totalement oubliés : d’une part, nous pouvons nous passer de biens industriels, mais pas de nourriture ; d’autre part, l’agriculture est la seule\nsource durable de richesse des nations, contrairement à l’industrie. Si l’humanité ne cesse de s’appauvrir en éventrant la terre pour lui prendre ses minerais et son énergie fossile, elle peut s’enrichir en cultivant la terre selon les principes de\nl’agriculture durable.\nCe petit manifeste souhaite faire face à ces préoccupations en redonnant à l’agriculture le rôle central qu’elle a toujours occupé dans les civilisations. Il nous explique comment nous avons détruit notre agriculture et propose des solutions pour faire évoluer l’agriculture française, en la rendant à la fois durable et qualitative.','\n',char(10)));
INSERT INTO post VALUES(2,2,'Comment tout peut s’effondrer','Comment tout peut s’effondrer	Et si notre civilisation s’effondrait ? Non pas dans plusieurs siècles, mais de notre vivant. Loin des prédictions Maya et autres eschatologies millénaristes, un nombre croissant d’auteurs, de scientifiques et d’institutions annoncent la fin de la civilisation industrielle telle qu’elle s’est constituée depuis plus de deux siècles. Que faut-il penser de ces sombres prédictions ? Pourquoi est-il devenu si difficile d’éviter un tel scénario ?');
INSERT INTO post VALUES(3,2,'Petit traité de résilience locale',replace('Dans les années à venir, nous devrons faire face aux impacts du réchauffement climatique, à la dégradation accélérée de notre environnement et à la raréfaction des ressources qui maintiennent notre civilisation en vie, trois bouleversements qui vont s’accompagner de profonds changements sociétaux.\n\nIl devient urgent de s’y préparer pour infléchir les trajectoires en développant notre résilience, cette capacité des êtres et des systèmes socio-écologiques à absorber les chocs et à se transformer. Mot d’ordre du mouvement des villes en transition, mobilisateur pour certains, synonyme de résignation pour d’autres, la résilience comprend plusieurs facettes. Elle s’adresse à la fois aux individus, aux collectivités et aux élus locaux qui sont en première ligne pour maintenir les fondamentaux de notre société : santé, alimentation, transport, gestion des ressources vitales, énergie, habitat.\n\nLoin de prôner le repli sur soi, les stratégies de résilience encouragent le partage, la coopération, l’autonomie créatrice et l’imagination de tous les acteurs locaux. Les auteurs de ce livre nous offrent une boussole qui pourrait s’avérer bien utile pour traverser ce siècle sans chavirer.','\n',char(10)));
INSERT INTO post VALUES(4,1,'1984','De tous les carrefours importants, le visage à la moustache noire vous fixait du regard. BIG BROTHER VOUS REGARDE, répétait la légende, tandis que le regard des yeux noirs pénétrait les yeux de Winston... Au loin, un hélicoptère glissa entre les toits, plana un moment, telle une mouche bleue, puis repartit comme une flèche, dans un vol courbe. C''était une patrouille qui venait mettre le nez aux fenêtres des gens. Mais les patrouilles n''avaient pas d''importance. Seule comptait la Police de la Pensée.');
CREATE TABLE IF NOT EXISTS "tag" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
	post_id INTEGER NOT NULL,
        name TEXT NOT NULL,
	FOREIGN KEY(post_id) REFERENCES post(id)
);
INSERT INTO tag VALUES(1,1,'agriculture');
INSERT INTO tag VALUES(2,1,'durable');
INSERT INTO tag VALUES(3,1,'manifeste');
INSERT INTO tag VALUES(4,2,'collapsologie');
CREATE TABLE IF NOT EXISTS "comment" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
	user_id INTEGER NOT NULL,
        message TEXT NOT NULL,
	FOREIGN KEY(user_id) REFERENCES user(id)
);
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('user',3);
INSERT INTO sqlite_sequence VALUES('post',4);
INSERT INTO sqlite_sequence VALUES('tag',4);
COMMIT;
