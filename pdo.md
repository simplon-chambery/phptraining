# PDO

Création d'une base de données SQLite et insertion des données à partir du fichier *data/pdo.sql*.
L'ensemble des requêtes SQL doivent être éxécutées via PHP et PDO.

## Exercice 1
Afficher le nom de famille de **Claude** (sans utiliser la colonne ID).

## Exercice 2
Afficher les entrées de la table *post* appartenant à **Claude Bourguinon**.

## Exercice 3
Insérer votre prénom et nom dans la table *user*.

## Exercice 4
```php
$comments = [
    'Ha ha ha, attention je vous observe !',
    'Le ministère de la vérité a filtré quelques commentaires...'
];
```
Insérer les commentaires listés ci-dessus associés à Georges Orwell.

## Exercice 5
Renomner **Claude Bourguinon** en **Lydia Bourguinon** (sans utiliser la colonne ID).

## Exercice 6
A partir du titre **Manifeste pour une agriculture durable** supprimer les entrées dans la table *tag* associées au *post* en question.

## Exercice 6
Compter le nombre d'entrées dans la table *post* appartenant à **Pablo Servigne**.

## Exercice 7
Afficher les entrées de la table *post* contenant le terme **civilisation**.

## Exercice 8
En une seule requête SQL afficher le prénom et le nom de la personne associée au *post* avec le titre **1984**.
