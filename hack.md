# Hack

Création d'une base de données SQLite qui sera nommée *hack.db* à placer dans le répertoire *src* à partir du fichier *data/pdo.sql*.

Deux équipes de 2 personnes s'affrontent, l'équipe **white hat hackers** doit sécuriser le script avant que l'équipe **black hat hackers** pirate la base de données.